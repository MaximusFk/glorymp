package ua.glorymp.database.entities;

import android.content.ContentValues;
import java.io.File;

import ua.glorymp.database.ContentValuesSerializable;

/**
 * Created by maximusfk on 09.05.16.
 */
public class AudioTrackEntity implements ContentValuesSerializable {

    public final static String TABLE = "Audiotracks";
    public final static String ENTRY_ID = "_id";
    public final static String PATH = "_path";

    private long id;
    private File audioFile;

    AudioTrackEntity(long id) {
        this.id = id;
    }

    AudioTrackEntity(long id, File audioFile) {
        this.id = id;
        this.audioFile = audioFile;
    }

    @Override
    public ContentValues getContent() {
        return null;
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getPriorityIdKey() {
        return ENTRY_ID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + "=" + id;
    }
}
