package ua.glorymp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by maximusfk on 09.05.16.
 */
public class PlayerDatabase extends SQLiteOpenHelper {

    public final static int DATABASE_VERSION = 0;

    private static final String CREATE_TABLE = "CREATE TABLE ";
    private static final String COMA = " , ";

    public PlayerDatabase(Context context, String db) {
        super(context, db, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
