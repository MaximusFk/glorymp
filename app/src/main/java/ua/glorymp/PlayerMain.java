package ua.glorymp;

import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.util.Queue;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PlayerMain extends ActionBarActivity implements View.OnClickListener {

    MediaPlayer mediaPlayer = null;
    private ImageButton play;
    private ImageButton next;
    private ImageButton prev;

    Queue<String> queuePlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_player_main);
        play = (ImageButton) findViewById(R.id.play_button);
        next = (ImageButton) findViewById(R.id.next_button);
        prev = (ImageButton) findViewById(R.id.previous_button);
        play.setOnClickListener(this);
        next.setOnClickListener(this);
        prev.setOnClickListener(this);
    }

    private void pause() {

    }

    private void stop() {

    }

    private void play() {

    }

    private void next() {

    }

    private void prev() {

    }

    private void seek(int msec) {
        if(mediaPlayer != null) {
            mediaPlayer.seekTo(msec);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.play_button:
                play();
                break;
            case R.id.next_button:
                next();
                break;
            case R.id.previous_button:
                prev();
                break;
        }
    }
}
